new Vue({
    el: '#app',
    data: {
        searchKey: '',
    
        user: {
          id: '',
        name: '',
        email: '',
        avatar: '',
        edit: false
        },

        users: [],

        errors: [],

    
    },

    mounted() {
        axios.get('https://jsonplaceholder.typicode.com/users')
            .then(response => this.users = response.data);
     },

     computed: {
       filteredUsers() {
        var self = this;
          
        if(!self.searchKey) {
            return self.users;
          }
          return self.users.filter(user => user.name.indexOf(self.searchKey) !== -1);
       }
     },

    methods: {   
        addUser() {
        
            this.errors = [];
            
            if (!this.user.name) {
              this.errors.push('You have to enter the name.');
            
              return;
            }

            if (!this.user.email) {
              this.errors.push('You have to enter the email.');
              
              return;
            }
                  
            lastUser = this.users.length -1;
            this.user.id = this.users[lastUser].id + 1;
                    
            this.users.push(this.user);
            
            this.user = [];
          
        },

        addAvatar(e) {
            const file = e.target.files[0];
            this.user.avatar = URL.createObjectURL(file);
            
        },
      
        deleteUser(index) {
            this.users.splice(index, 1);
        }
    }

});    